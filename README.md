get-video-mime
==============
When trying to use Media Source Extensions you often need to know the full MIME type for the video
that you are working with. I wasn't able to find any easy solutions, so I put this together.

Usage
-----
Just run this command and pass it any number of video files.

```
$ npm i -g get-video-mime
$ get-video-mime video-file.mp4

OR

$ npx get-video-mime video-file.mp4 another-file.mp4
```

This should yield output like this:

```
[22:28:37] Getting MIME for 1 file(s)...
[22:28:38] MIME for video-file.mp4 detected as: video/mp4; codecs="hev1.1.6.L120.90,mp4a.6b"; profiles="isom,iso2,mp41"
[22:28:38] Finished processing all files.
```

You can then use this string in your JS code like this:

```js
const mediaSource = new MediaSource;
mediaSource.isTypeSupported('video/mp4; codecs="hev1.1.6.L120.90,mp4a.6b"; profiles="isom,iso2,mp41"');
```

Inspiration
-----------
This technique was based largely on
[this article](https://medium.com/@JackPu/how-js-get-video-codec-548a33cf7454). And, of course, this
is just at thin wrapper around [mp4box.js](https://github.com/gpac/mp4box.js/) which does all of the
heavy lifting.
