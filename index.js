#!/usr/bin/env node

/**
 * This is a simple CLI tool to detect the proper MIME string of a given video file
 * for use with MSE.
 *
 * Usage:
 * get-video-mime video.mp4 [another-video.mp4 ...]
 */
const { promisify } = require('util');
const { pipeline } = require('stream');
const { createReadStream } = require('fs');
const log = require('fancy-log');
const DetectMIME = require('./lib/detect-mime');

const files = process.argv.slice(2);

if (! files || files.length < 1) {

    log.error('You must specify a video file to check: get-video-mime video-file.mp4');
    process.exit(1);
}

const pipelinePromise = promisify(pipeline);

/**
 * Process a single video file and attempt to determine its MIME type.
 *
 * @param {String} file The path to the file to process
 *
 * @returns {Promise} A Promise that resolves with undefined when finished
 */
const processFile = (file) => {

    let mime = '';

    const setMime = (newMime) => { mime = newMime; };

    return pipelinePromise(createReadStream(file), new DetectMIME(setMime))
        .then(() => {

            if (! mime) {

                log.error('Failed to detect MIME for ' + file);
                return;
            }

            log('MIME for ' + file + ' detected as: ' + mime);
        })
        .catch((error) => {

            log.error('There was an error detecting the MIME for ' + file + '\n');
            log.error(error);
        });
};

// Do it
log('Getting MIME for ' + files.length + ' file(s)...');

Promise.all(files.map(processFile))
    .then(() => {

        log('Finished processing all files.');
    })
    .catch((error) => {

        log.error('There was an error processing the files.');
        log.error(error);

        process.exit(2);
    });
